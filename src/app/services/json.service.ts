import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EMPTY} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JsonService {

  constructor(private httpClient: HttpClient) {
  }

  public readJSON(jsonPath: string) {
    if (!jsonPath) {
      return EMPTY;
    }
    return this.httpClient.get(jsonPath);
  }
}
