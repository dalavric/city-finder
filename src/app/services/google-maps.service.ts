import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {GeographicalPoint} from '../models/geographicalPoint';
import {Observable} from 'rxjs';

declare const google: any;

@Injectable({
  providedIn: 'root'
})
export class GoogleMapsService {

  key = 'AIzaSyBL3LlzFKklXL3gv1-Z7B8jqx4SqtYoe88';

  constructor(private httpClient: HttpClient) {
  }

  getCoordinatesOfCity(cityName: string): Observable<any> {
    if (!cityName || cityName === '') {
      return null;
    }

    const params = new HttpParams()
      .set('address', cityName)
      .set('key', this.key);
    return this.httpClient.get('https://maps.googleapis.com/maps/api/geocode/json', {params});
  }

  getDistanceBetweenTwoPointsInKm(p1: GeographicalPoint, p2: GeographicalPoint): number {
    const origin = new google.maps.LatLng(p1.latitude, p1.longitude);
    const destination = new google.maps.LatLng(p2.latitude, p2.longitude);
    return google.maps.geometry.spherical.computeDistanceBetween(origin, destination) / 1000;
  }
}
