// @ts-ignore
import {GeographicalPoint} from './geographicalPoint';

export class City {
  name: string;
  coordinates: GeographicalPoint;

  constructor(name: string,
              lat: number,
              long: number) {
    this.name = name;
    this.coordinates = new GeographicalPoint(lat, long);
  }
}
