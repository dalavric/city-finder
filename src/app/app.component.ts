import {Component, OnInit} from '@angular/core';
import {JsonService} from './services/json.service';
import {City} from './models/city';
import {GoogleMapsService} from './services/google-maps.service';
import {forkJoin, Observable} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {GeographicalPoint} from './models/geographicalPoint';

declare const google: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'cityFinder';

  initialPointOfTheMap: GeographicalPoint = new GeographicalPoint(49.550447, 12.265236);

  citiesToGuess: City[] = [];
  cityToBeGuessed: City = null;
  noOfCitiesGuessed = 0;
  noOfCitiesPlaced = 0;
  remainingKm = 1500;
  kmOffset = 0;

  mapStyle = null;
  EUROPE_BOUNDS = {
    latLngBounds: {
      north: 71.607999,
      south: 35.674420,
      west: -30.396217,
      east: 45.804947,
    },
    strictBounds: true
  };

  mapMarkers: { lat: number, long: number, label: string }[] = [];
  intractableMap = true;

  chosenPoint: GeographicalPoint = null;
  reset = false;


  constructor(private jsonService: JsonService,
              private googleMapsService: GoogleMapsService) {
  }

  ngOnInit() {
    this.jsonService.readJSON('../assets/googleMapStyle.json').subscribe(value => {
      this.mapStyle = value;
    });
    this.populateCitiesArray();
  }

  onMapReady(mapInstance: any) {
    mapInstance.setOptions({
      zoomControl: 'true',
      zoomControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM
      }
    });
  }

  populateCitiesArray() {
    this.jsonService.readJSON('../assets/capitalCities.json').pipe(
      switchMap((cities: any) => {
        const requests: Observable<any>[] = [];
        if (cities) {
          cities.capitalCities.forEach(capitalCity => {
            requests.push(this.googleMapsService.getCoordinatesOfCity(capitalCity.capitalCity));
          });
        }
        return forkJoin(requests);
      })).subscribe(results => {
      results.forEach(cityInfo => {

        if (cityInfo.results && cityInfo.results.length > 0) {
          const cityName = cityInfo.results[0].address_components[0].long_name;
          const coordinates: { lat: number, lng: number } = cityInfo.results[0].geometry.location;
          this.citiesToGuess.push(new City(cityName, coordinates.lat, coordinates.lng));
        }
      });
      if (this.citiesToGuess && this.citiesToGuess.length > 0) {
        this.cityToBeGuessed = this.citiesToGuess[0];
      }
    });
  }

  mapClick($event: any) {
    if (this.intractableMap) {
      this.mapMarkers = [];
      const coordinates = $event.coords;
      this.mapMarkers.push({lat: coordinates.lat, long: coordinates.lng, label: 'Choice'});
      this.chosenPoint = new GeographicalPoint(coordinates.lat, coordinates.lng);
    }
  }

  lockAnswer() {
    this.intractableMap = false;
    this.mapMarkers.push({
      lat: this.cityToBeGuessed.coordinates.latitude,
      long: this.cityToBeGuessed.coordinates.longitude,
      label: this.cityToBeGuessed.name
    });
    this.kmOffset = this.googleMapsService.getDistanceBetweenTwoPointsInKm(this.cityToBeGuessed.coordinates, this.chosenPoint);
    if (this.kmOffset > 50) {
      this.remainingKm -= this.kmOffset;
    } else {
      this.noOfCitiesGuessed++;
    }
    this.noOfCitiesPlaced++;
    if (this.remainingKm < 0) {
      this.remainingKm = 0;
      this.reset = true;
    }
  }

  next() {
    this.intractableMap = true;
    this.mapMarkers = [];
    this.chosenPoint = null;
    let newCityIndex = this.noOfCitiesPlaced;
    if (this.noOfCitiesPlaced >= this.citiesToGuess.length) {
      newCityIndex = this.noOfCitiesPlaced % this.citiesToGuess.length;
    }
    this.cityToBeGuessed = this.citiesToGuess[newCityIndex];
  }

  resetGame() {
    this.intractableMap = true;
    this.reset = false;
    this.mapMarkers = [];
    this.chosenPoint = null;
    this.cityToBeGuessed = this.citiesToGuess[0];
    this.noOfCitiesPlaced = 0;
    this.noOfCitiesGuessed = 0;
    this.remainingKm = 1500;
  }
}
